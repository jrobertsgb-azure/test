resource "azurerm_public_ip" "test" {
  name                         = "test"
  location                     = "${data.terraform_remote_state.core.location}"
  resource_group_name          = "${data.terraform_remote_state.core.name}"
  public_ip_address_allocation = "static"
}

resource "cloudflare_record" "dns" {
  domain = "jroberts.me.uk"
  name   = "test"
  value  = "${azurerm_public_ip.test.ip_address}"
  type   = "A"
  proxied = true
  ttl    = 1
}

resource "azurerm_network_security_group" "test" {
  name                = "test"
  location            = "${data.terraform_remote_state.core.location}"
  resource_group_name = "${data.terraform_remote_state.core.name}"

  security_rule {
    name                       = "HTTP"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_network_interface" "test" {
  name                      = "test"
  location                  = "${data.terraform_remote_state.core.location}"
  resource_group_name       = "${data.terraform_remote_state.core.name}"
  network_security_group_id = "${azurerm_network_security_group.test.id}"

  ip_configuration {
    name                          = "myNicConfiguration"
    subnet_id                     = "${data.terraform_remote_state.core.subnet}"
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = "${azurerm_public_ip.test.id}"
  }
}
