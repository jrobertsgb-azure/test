resource "azurerm_virtual_machine" "test" {
  name                  = "test"
  location              = "${data.terraform_remote_state.core.location}"
  resource_group_name   = "${data.terraform_remote_state.core.name}"
  network_interface_ids = ["${azurerm_network_interface.test.id}"]
  delete_os_disk_on_termination = true

  vm_size = "Standard_B1s"

  storage_image_reference  {
    publisher = "CoreOS"
    offer     = "CoreOS"
    sku       = "Stable"
    version   = "latest"
  }

  storage_os_disk {
    name              = "test"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "test"
    admin_username = "core"
  }

  os_profile_linux_config {
    disable_password_authentication = true

    ssh_keys {
      path     = "/home/core/.ssh/authorized_keys"
      key_data = "${data.terraform_remote_state.core.public_key}"
    }
  }

  provisioner "remote-exec" {
    inline = [
      "sudo docker run -d --name nginx -p 80:80 nginx",
      "sudo docker run -d --name watchtower -v /var/run/docker.sock:/var/run/docker.sock v2tec/watchtower"
    ]

    connection {
      user = "core"
      host = "${azurerm_network_interface.test.private_ip_address}"
      bastion_host = "${data.terraform_remote_state.bastion.host}"
      private_key = "${data.terraform_remote_state.core.private_key}"
    }
  }
}
